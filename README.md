Hello World.
===
### <U>About me:</U>
* I'm a nobody.
* I like open source & Computers.
### <U>Some Stuff I made:</U>
* A Script to generate [Video Stripe Preview][Video-Stripe-Preview]

[Video-Stripe-Preview]: https://gitlab.com/red-scripts/video-stripe-generator "Video Stripe Generator gitlab repo."
